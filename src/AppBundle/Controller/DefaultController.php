<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('main/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/druga", name="druga")
     */
    public function drugaStronaAction(Request $request){

        // replace this example code with whatever you need
        return $this->render('strony/druga.html.twig', [
        ]);

    }

    /**
     * @Route("/jedzenie", name="zarcie")
     */
    public function mniamAction(Request $request){

        // replace this example code with whatever you need
        return $this->render('strony/jedzenie.html.twig', [
        ]);

    }

    /**
     * @Route("/about", name="about")
     */
    public function aboutAction(Request $request){

        // replace this example code with whatever you need
        return $this->render('strony/about.html.twig', [
        ]);

    }

    /**
     * @Route("/services", name="services")
     */
    public function servicesAction(Request $request){

        // replace this example code with whatever you need
        return $this->render('strony/services.html.twig', [
        ]);

    }

      /**
     * @Route("/contact", name="contact")
     */
    public function contactAction(Request $request){

        // replace this example code with whatever you need
        return $this->render('strony/contact.html.twig', [
        ]);

    }
}
